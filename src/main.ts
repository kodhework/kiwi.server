import {parse} from "gitlab://jamesxt94/codes@465eb5ab/cli-params.ts"
import {Program as Program2} from './server.ts'
import {Manager, Config, Program as ManagerProgram} from 'github://kwruntime/dynw@6608707/manager.ts'
// "/data/projects/Kodhe/kwruntime/dynw/manager.ts" 
// 'github://kwruntime/dynw@1.0.1/manager.ts'
import Os from 'os'
import {HttpContext} from '/data/projects/Kode/kwruntime/std/http/context.ts'
import Path from 'path'
import Url from 'url'
import {kawix} from "github://kwruntime/core@68f0642/src/kwruntime.ts"
import crypto from 'crypto'
import fs from 'fs'
import * as async from "github://kwruntime/std@1.1.19/util/async.ts"
import {Exception} from "github://kwruntime/std@1.1.19/util/exception.ts"
//import {Client} from 'gitlab://jamesxt94/mesha@3a6411d/Messaging.ts' 
import { RPC } from 'gitlab://jamesxt94/mesha@2.0.3/src/rpc.ts'
import {Client} from 'gitlab://jamesxt94/mesha@2.0.3/src/clients/default.ts'
import { Site } from "./site"
import { spawn } from "child_process"


import {TmuxActions} from "./tmux-manager.ts"

export var ServerProgram = Program2


let share = Symbol("share")
export class RemoteExecutor{
    static rpc: RPC
    static async connectMesha(address: string, password: string){
		const client = new Client()
		await client.connect(address)


		client.on("error", (e)=> console.error("[KiwiServer] RPC Error", e.message))

		let rpc = client.rpc
		rpc.on("error", (e)=> console.error("[KiwiServer] RPC Error", e.message))

		RemoteExecutor.rpc = rpc 
		const start = await rpc.defaultScope.remote("GetObject")
		return await start.invoke(password)
	}
}


export class Program{
	manager: Manager
	sitesById = new Map<string, any>()
	master: Program 

	#config: any 
	#clusters = new Array<Program2>()
	#sitesByConfig = new WeakMap<any, Site>()

	static #default: Program
	static startupMode =  "default"

	static get default(){
		if(!this.#default){
			this.#default = new Program()
		}
		return this.#default
	}
	
	
	static async main(args: Array<string>){
		Program.default.main(args)
	}

	static get typeDefinition(){
        return Path.join(__dirname, "types", "main")
    }

	constructor(){
		this[share] = true 
	}

	
	get config(){
		return this.#config
	}

	async main(args: Array<string>){
		if(process.env.RUN_DYNW == "1"){
			return await ManagerProgram.main()
		}


		let cli = parse(args)
		if(cli.params.base64){
			let params2 = JSON.parse(Buffer.from(cli.params.base64,"base64").toString())
			Object.assign(cli.params, params2)
		}
		

		if(cli.params.start !== undefined){
			// start detached 
			let newparams = Object.assign({}, cli.params)
			delete newparams.start
			let base64 = Buffer.from(JSON.stringify(newparams)).toString("base64")

			let p = spawn(process.argv[0], [process.argv[1], process.argv[2], "--base64=" + base64], {
				stdio:'ignore',
				detached: true
			})
			p.unref()

			console.info("Starting kiwi.server in background.")
			console.info("You can check the status later")
			return 
		}


		if( [cli.params.show,cli.params.control,cli.params.tmux,cli.params.reload,cli.params.restart,cli.params.close].filter((a) => a!==undefined).length ) {
			
			//cli.params.show !== undefined) || (cli.params.control !== undefined) || (cli.params.reload !== undefined) || (cli.params.restart !== undefined)){
			await TmuxActions.execute(cli.params)
			return 
		}

		
		let cpus = Os.cpus().length
		if(cli.params.folder){
			// folder where is saved the modules
			let folder = Path.resolve(process.cwd(), cli.params.folder)
			let md5 = "config-" + crypto.createHash("md5").update(folder).digest("hex") + ".ts"
			let file = Path.join(Os.homedir(), ".kawi", "user-data")
			if(!fs.existsSync(file)) fs.mkdirSync(file)
			file = Path.join(file, "com.kodhe.kiwiserver")
			if(!fs.existsSync(file)) fs.mkdirSync(file)
			file = Path.join(file, md5)


			let content = `
			
			import Path from 'path'
			import fs from 'fs'
			export var folder = ${JSON.stringify(folder)}
			export var includes = new Array<string>()

			
			function firstTime(){
				let files = fs.readdirSync(folder)
				addIncludes(files)
			}

			async function interval(){
				try{
					let files = await fs.promises.readdir(folder)
					addIncludes(files)
				}catch(e){
					console.error("[kiwiserver] Config reload failed:", e.message)
				}
			}

			function addIncludes(files: Array<string>){
				includes.splice(0,includes.length)
				files = files.filter((a) => a.endsWith(".ts") || a.endsWith(".kwc") || a.endsWith(".kwb"))
				for(let file of files){
					includes.push(Path.join(folder, file))
				}
			}


			firstTime()
			setInterval(interval, 10000)
			`;

			await fs.promises.writeFile(file, content)
			cli.params.sitesConfig = file 
			//cli.params.folder = folder 
			delete cli.params.folder
		}
		process.env.KIWI_SERVER_CLI_BASE64 = Buffer.from(JSON.stringify(cli.params)).toString('base64')


		if(cli.params.sitesConfig){			
			let url = new URL(cli.params.sitesConfig, Url.pathToFileURL(process.cwd()).href + "/").href
			let mod = await import(url)
			if(!mod.kawixDynamic){
				mod.kawixDynamic = {
					time: 10000
				}
			}
			let config = mod.default || mod.config || mod 
			Object.assign(cli.params, config)
			cli.params.sitesConfigUrl = url 
		}


		if(cli.params.clusters){
			cpus = Number(cli.params.clusters)
		}
		let config:Config = {
			id: cli.params.id || "default",
			cpus,
			host: cli.params.host || "0.0.0.0",
			port: Number(cli.params.port) || 8080
		}
		if(cli.params.httpsPort){
			config.https_port = Number(cli.params.httpsPort)
		}
		if(cli.params.rootMode){
			config.startup = {
				asroot: true 
			}
		}
		if(cli.params.names){
			config.names = cli.params.names
		}
		if(cli.params.hosts){
			config.hosts = cli.params.hosts
		}
		if(cli.params.ssl){
			config.ssl = cli.params.ssl
		}
		
		process.env.KIWI_SHARE_ID = "kiwi-" + (cli.params.id || "default")
		let info = this.shareProcessInfo(cli.params.id || "default")
		process.env.KIWI_SHARE_ADDRESS = info.address
		process.env.KIWI_SHARE_PASSWORD = info.password

		this.#config = config 
		let manager = this.manager = await Program.default.createServerManager(config)
		manager[share] = true 


		/* Se ejecuta en todos los clusters */
		await manager.setStartupHandler({
			url: import.meta.url,
			exportName: 'prepareServer'
		})
		await manager.startServer(Program.startupMode == "built" ? __filename : undefined)
		await this.startShareProcess(cli.params)		
		manager.router.addDefault({
			path: '*',
			module: {
				url: import.meta.url,
				exportName: 'defaultHandler'
			}
		})
	}

	shareProcessInfo(id){
		
		let address = '', shareId = "kiwi-" + id
		let password = crypto.createHash('md5').update(shareId).digest("hex")
		if(Os.platform() == "win32"){
			address = "\\\\.\\pipe\\" + password
		}
		else{
			/*
			let folder = Path.join(Os.homedir(), ".kawi")
			folder = Path.join(folder, "user-data")
			if(!fs.existsSync(folder)) fs.mkdirSync(folder)
			folder = Path.join(folder, "kiwi.server")
			if(!fs.existsSync(folder)) fs.mkdirSync(folder)
			address = "unix://" + Path.join(folder, shareId + ".socket")
			*/
			address = "local+" + shareId
		}

		return {
			address,
			password
		}
	}


	

	async startCluster(program: Program2){

		this.#clusters.push(program)

		let sites = [...this.sitesById.values()]
		if(sites.length){
			await program.addSitesByURL(sites.map((a) => {
				return {
					url: a.url,
					root: a.resolved
				}
			}))
		}
	}


	executeSiteDynamicMethod(siteId: string, method: string, params: any){
		let site = this.getSiteById(siteId)
		let sites = this.getSites()
		let ctx = site.getKiwiContext({
			sites, 
			context: {}
		})
		return ctx.dynamicMethod(method).invoke(params)
	}


	executeSiteFunction(siteId: string, func: string, params: any){
		let site = this.getSiteById(siteId)
		let sites = this.getSites()
		let ctx = site.getKiwiContext({
			sites, 
			context: {}
		})
		return ctx.userFunction(func).invoke(params)
	}

	getSiteById(id: string){
		if(id == "kowix") return Site.kowixSite		
		let config = this.sitesById.get(id)
		if(!config){
			throw Exception.create(`Site ${id} not found`).putCode("SITE_NOT_FOUND")
		}

		let site = this.#sitesByConfig.get(config)
		if(!site){
			site = new Site(config.site)
			site.root = config.resolved  
			this.#sitesByConfig.set(config, site)
		}
		return site 
	}


	getSites(){
		let siteids = this.sitesById.keys()
		let sites = new Array<Site>()
		sites.push(Site.kowixSite)
		for(let id of siteids){
			let site = this.getSiteById(id)
			sites.push(site)
		}
		return sites 
	}



	async startCron(){
		let times = 0
		while(true){

			await async.sleep(5000)
			let time = Date.now() 
			let sites = this.getSites()
			times++
			let offset = 0
			for(let site of sites){
				
				try{
					if(site.config.cron){
						let func = site.config.cron.method || site.config.cron.file 
						if(typeof site.config.cron == "string"){
							func = site.config.cron 
						}
						let time = site.config.cron.interval || 1
						if(times % time == 0){
							console.info("[KiwiServer] Executing cron on site:", site.config.id)
							let local = await site.getKiwiContext({
								sites,
								context: {}
							})
							
							try{
								let result = await local.userFunction("vars").invoke({})
								local.site["$_constants"] = result.constants || result
							}catch(e){

							}							
							await local.dynamicMethod(func).invoke()
						}
					}
				}catch(e){
					console.error("[KiwiServer] Failed execute cron on site:", site.config.id, e.stack)
				}
			}


			let towait = Math.min(10000, 55000 - (Date.now() - time))
			await async.sleep(towait)

		}
		
	}


	async startReadConfig(url: string){

		
		if(Path.isAbsolute(url) || (url.indexOf(":") < 0)){
			url = Url.pathToFileURL(url).href
		} 
	
		let updated = 0
		let sitesById = this.sitesById 

		while(true){
			try{
				let mod = await import(url)
				if(!mod.kawixDynamic){
					mod.kawixDynamic = {
						time: 10000
					}
				}
				let config = mod.default || mod.config || mod 
				let lupdated = Date.now() 
				
				if(!(config.preload instanceof Array)){	
					config.preload = [url]
				}			
				for(let url of config.preload){
					for(let i=0;i< this.#clusters.length;i++){
						let program = this.#clusters[i]
						if(!program) continue
						try{
							await program.preload(url)
						}catch(e){
							if((e.code == "RPA_DESTROYED") || (e.code == "RPC_DESTROYED")){
								this.#clusters[i] = null
							}
							else{
								console.error("[kiwiserver] Error in cluster update:", e.message)
							}
						}
					}
					this.#clusters = this.#clusters.filter(Boolean)
				}	


				
				if(config.includes instanceof Array){

					let currentSiteIds = []
					for(let include of config.includes){
						let resolved = new URL(include, url).href
						try{
							console.info("Resolved:", resolved)
							let nmod = await import(resolved)
							if(!nmod.kawixDynamic){
								nmod.kawixDynamic = {
									time: 10000
								}
							}
							console.info(nmod.kawixDynamic)


							if(nmod.site){
								currentSiteIds.push(nmod.site.id)
								nmod.url = resolved 
								let parts = resolved.split("/")
								parts.pop()								
								nmod.resolved = parts.join("/")
								
								if(nmod._kiwi === undefined){
									nmod._kiwi = {
										updated: lupdated
									}
								}

								sitesById.set(nmod.site.id,nmod)
							}
						}catch(e){
							console.error("[KiwiServer] Failed load site ", include + ":", e.message)
						}
					}

					let keys = [...sitesById.keys()]
					let deleted = [], added = []
					for(let key of keys){
						if(currentSiteIds.indexOf(key) < 0){
							deleted.push(sitesById.get(key))
							sitesById.delete(key)
						}
					}
					for(let [key, value] of sitesById){
						if(value._kiwi?.updated > updated){
							added.push(value)
						}
					}



					if(added.length){
						console.info("[kiwiserver] Sitios añadidos:", added.map((a) => a.site.id))
						// push to clusters 
						for(let i=0;i< this.#clusters.length;i++){
							let program = this.#clusters[i]
							if(!program) continue
							try{
								await program.addSitesByURL(added.map((a) => {
									return {
										url: a.url,
										root: a.resolved
									}
								}))
							}catch(e){
								if((e.code == "RPA_DESTROYED") || (e.code == "RPC_DESTROYED")){
									this.#clusters[i] = null
								}
								else{
									console.error("[kiwiserver] Error in cluster update:", e.message)
								}
							}
						}
						this.#clusters = this.#clusters.filter(Boolean)
					}
					if(deleted.length){
						console.info("[kiwiserver] Sitios removidos:", deleted.map((a) => a.site.id))
						// push to clusters 
						for(let i=0;i< this.#clusters.length;i++){
							let program = this.#clusters[i]
							if(!program) continue
							try{
								await program.deleteSites(deleted.map((a) => a.site.id))
							}catch(e){
								if((e.code == "RPA_DESTROYED") || (e.code == "RPC_DESTROYED")){
									this.#clusters[i] = null
								}
								else{
									console.error("[kiwiserver] Error in cluster update:", e.message)
								}
							}
						}
						this.#clusters = this.#clusters.filter(Boolean)
					}




				}
				updated = lupdated


				// update manager config if apply
				let sites = this.getSites()
				let names = new Set<string>()
				for(let site of sites){
					if(site.config.dynw?.names){
						for(let name of site.config.dynw.names){
							// add names to dynw
							names.add(name)
						}						
					}
				}
				try{
					
					await this.master.$execute(`
						try{
							let names = arguments[0]
							let anames = this.config.names || []
							let snames = new Set()
							for(let name of anames){
								snames.add(name)
							}
							for(let name of names){
								snames.add(name)
							}

							let arnames = [...snames]
							if(arnames.length){
								let current = (this.manager.config.names || []).sort().join(",")
								let newi = arnames.sort().join(",")
								if(newi != current){
									this.manager.config.names = arnames
									this.manager.updateChanges()
								}
							}
						}catch(e){
							console.error("> Failed to update dynw config:", e)		
						}
					
					`, [...names])
				}catch(e){
					console.error("> Failed to update dynw config:", e)
				}


			}
			catch(e){
				console.error("Failed to get config url:", e.message)	
			}finally{
				await async.sleep(10000)
			}
		}
	}





	async startShareProcess(params: any){
		let info = this.shareProcessInfo(params.id || "default")
		let pro = await this.manager.client.createProcess({
			autorestart: true,
			id: "share",
			cmd: process.argv[0],
			args: [kawix.filename, "gitlab://jamesxt94/codes@2646bb83/remote.ts", "--", "password=" + info.password, "mesha-bind=" + info.address],
			env: {
				KIWI_SHARE_PROCESS: "1",
				KIWI_SERVER_CLI_BASE64: process.env.KIWI_SERVER_CLI_BASE64
			}
		})
		await pro.start()


		let configUrl = params.sitesConfigUrl
		let startRPCShare = async () => {
			let remote = null 
			while(true){
				try{
					remote = await RemoteExecutor.connectMesha(info.address, info.password)
					break 
				}catch(e){
					await async.sleep(1000)
				}
			}
			let str = `
				var url = arguments[0]
				return (async function(){
					
					let mod = await kawix.import(url)
					global.kiwiProgram = mod.Program.default
					return mod.Program.default
					
				})();
			`
			let program:Program = await remote.execute(str, [Program.startupMode == "built" ? __filename : import.meta.url])
			// start read config 
			
			await program.$setMaster(this)
			program.startReadConfig(configUrl)
			program.startCron()
		}

		await startRPCShare()
		RemoteExecutor.rpc.on("close", startRPCShare)
		RemoteExecutor.rpc.on("error", (e) => console.error("[KiwiServer] Error on rpc server:", e.message))
	}

	$setMaster(value){
		this.master = value
	}

	$execute(code: string, params: any){
		
		let func = Function(code)
		return func.call(this, params)
	}


	#getStr(func:Function){
        let str = func.toString()
        str = str.substring(str.indexOf("{")+1)
        str = str .substring(0, str.lastIndexOf("}"))
        return str 
    }

	getStr(func:Function){
        let str = func.toString()
        str = str.substring(str.indexOf("{")+1)
        str = str .substring(0, str.lastIndexOf("}"))
        return str 
    }


	async createServerManager(config: Config){
		if(!config.cpus){
			config.cpus = Os.cpus().length
		}
		let manager = new Manager(config )
		manager.on("cluster-address", function(event){
			console.info("> Subprocess:", event.id, "Listening on:", event.address)
		})
		manager.on("tmux-started", function(){
			console.info("> Tmux started")
		})
		manager.on("caddy-started", function(){
			console.info("> Caddy web server started")
		})
		manager.on("error", function(e){
			console.info("> Error on service:", e)
		})
		return manager
	}

}

Program[share] = true

let program: Program2 

export async function prepareServer(){

	if(!program){
		program = new Program2()
		
		
		let remote = null 
		while(true){
			try{
				remote = await RemoteExecutor.connectMesha(process.env.KIWI_SHARE_ADDRESS, process.env.KIWI_SHARE_PASSWORD)
				break 
			}catch(e){
				await async.sleep(100)
			}
		}

		let str = `
			var url = arguments[0]
			return (async function(){
				var mod = await global.kawix.import(url)
				global.kiwiProgram = mod.Program.default
				return mod.Program.default
			})();
        `

		//console.info("Meta url:", __filename, program)


		let program1:Program = await remote.execute(str, [Program.startupMode == "built" ? __filename : import.meta.url])
		await program1.startCluster(program)
		

	}

	process.on('uncaughtException', (error) => {
		console.error("Excepción no controlada:", error)
	})
}
export async function defaultHandler(ctx: HttpContext){
	return program.handle(ctx)
}