import {HttpContext} from '/data/projects/Kode/kwruntime/std/http/context.ts'
import {Router} from '/data/projects/Kode/kwruntime/std/http/router.ts'
import {Static} from '/data/projects/Kode/kwruntime/std/http/static.ts'
import {context as publicContext} from './vars.ts'
import {kawix} from "github://kwruntime/core@b9d4a6a/src/kwruntime.ts"
import uniqid from 'npm://uniqid@5.4.0'
import {Exception} from "github://kwruntime/std@4fe87b1/util/exception.ts"

// import * as async from "github://kwruntime/std@4fe87b1/util/async.ts"

import Url from 'url'
import fs from 'fs'
import Path from 'path'
import Os from 'os'


//import handler from 'npm://serve-handler@6.1.3'

import * as kowixConfig from '../kowix/config.ts'



let userDataFolder = Path.join(Os.homedir(), ".kawi", "user-data")
if(!fs.existsSync(userDataFolder)) fs.mkdirSync(userDataFolder)
userDataFolder = Path.join(userDataFolder, "com.kodhe.kiwi")
if(!fs.existsSync(userDataFolder)) fs.mkdirSync(userDataFolder)


export class Site{

	config: any 
	root: string 
	router: Router
	userFolder: string 

	static kowixSite = new Site(kowixConfig.site)

	constructor(config){
		this.config = config 
		this.root = config.root 
		this.userFolder = Path.join(userDataFolder, this.config.id)
		if(!fs.existsSync(this.userFolder)) fs.mkdirSync(this.userFolder)
		this.buildRouter()
	}

	get id(){
		return this.config.id 
	}

	check(ctx: HttpContext){
		let ok = false, split = 0, prefix = false
		if(this.config.globalprefixes){
			for(let prefix of this.config.globalprefixes){
				if(prefix instanceof RegExp){
					let match = ctx.request.uri.pathname.match(prefix)
					if(match.length){
						ok = true 
						split = match[0].length
						break 
					}
				}
				else{
					if(ctx.request.uri.pathname.startsWith(prefix)){
						ok = true
						split = prefix.length
						break 
					}
				}
			}
		}

		if(ok) prefix = true 

		if(!ok){
			if(this.config.hostnames){
				for(let hostname of this.config.hostnames){
					if(hostname instanceof RegExp){
						if(hostname.test(ctx.request.headers.host)){
							ok = true 
							break 
						}
					}
					else{
						if(ctx.request.headers.host.startsWith(hostname)){
							ok = true
							break 
						}
					}
				}
			}
		}

		return {
			ok,
			offset: split,
			prefix
		}

	}


	buildRouter(){
		if(!this.root) return 

		
		let router = new Router()

		for(let route of this.config.routes){
			let method = this.#createMethod(route)
			let pmethod = route.method || "all"

			
			if(pmethod == 'use'){
				router.use(route.path, method)
			}
			else{
				if(pmethod == "all"){
					router.all(route.path, method)
				}
				else{
					router.on(pmethod, route.path, method)
				}
			}
		}

		let file = "api.default"
		if(!this.config.defaultroute){
			this.config.defaultroute = {
				file: "src/functions",
				site: "kowix"
			}
		}
		router.use("", this.#createMethod(this.config.defaultroute))
		this.router = router
	}
	

	#createMethod(route){
		let m = null, middle = null 
		let root = this.root
		if(root.startsWith("file:")){
			// POSIX MODE
			root = new URL(root).pathname
			//root = Url.fileURLToPath(root)
		}
		if(route.middleware?.file){
			middle = async function(ctx: HttpContext, siteContext: SiteContext){
				return await siteContext.dynamicMethod(route.middleware.file).invoke({})
			}
		}

		if(route.static){

			let path = Path.posix.resolve(root, route.static)
			let staticObj = new Static(Object.assign({
				path,
				finishOnNotFound: true,
				cache: {
					enabled: true,
					seconds: 300
				}
			}, route.options || {}))

			m = async function(ctx: HttpContext, siteContext: SiteContext){
				if(middle) await middle(ctx, siteContext)

				if(ctx.type == "upgrade"){
					if(ctx.socket.writableEnded) return 
				}
				else if(ctx.reply.raw.writableEnded) return 
				
				/*
				let def = new async.Deferred<void>()
				handler(ctx.request.raw, ctx.reply.raw, {
					public: Path.resolve(root, route.static)
				})
				ctx.reply.raw.on("finish", def.resolve)
				await def.promise
				*/
				await staticObj.handle(ctx)
				
			}
		}

		if(route.file){
			m = async function(ctx: HttpContext, siteContext: SiteContext){
				if(middle) await middle(ctx, siteContext)

				if(ctx.type == "upgrade"){
					if(ctx.socket.writableEnded) return 
				}
				else if(ctx.reply.raw.writableEnded) return 

				let cContext = siteContext
				let method: DynamicMethod
				if(route.site){
					cContext = siteContext.getSiteContext("kowix")
					method = cContext.dynamicMethod(route.file)

					// original site Context
					method.siteContext = siteContext
				}
				else{
					method = cContext.dynamicMethod(route.file)
				}				
				return await method.invoke({})
			}
		}
		return async (ctx: HttpContext) => {
			/*
			if(ctx.request.data["_continue"] === undefined){
				ctx.request.data["_continue"] = []
			}
			ctx.request.data["_continue"].push(m)
			*/
			let res = await m(ctx, ctx["siteContext"])
			if(res === undefined) res = null

			if(ctx.type == "upgrade"){
				if(ctx.socket.writableEnded) return 
			}
			else if(!ctx.reply.raw.writableEnded){
				await ctx.reply.send(res)
			}
			
		}

	}


	getKiwiContext(data = null){
		return new SiteContext(this, data)
	}

}


export class SiteContext{
	site: Site 
	data: any 
	//constants:{[key:string]: any} = {}

	constructor(site: Site, data){
		this.site = site 
		this.data = data 
	}


	async preload(){
		if(this.site.config.preload){
			for(let name of this.site.config.preload){
				await this.dynamicMethod(name).invoke({})
			}
		}
	}


	addGlobal(name: string, value){
		publicContext[name] = value
		if(!(name in this)){
			Object.defineProperty(this, name, {
				enumerable: false
			})
		}
		this[name] = value 
	}

	dynamicMethod(name: string){
		// execute a function??
		let root = this.site.root

		let matchRemote = /https?\:|github\:|gitlab\:|npm\:/
		let url = null
		if(root.startsWith("file://")){
			// POSIX MODE
			root = new URL(root).pathname
			//root = Url.fileURLToPath(root)
		}
		if(!matchRemote.test(root)){
			// add file:// protocol 
			url = Path.posix.resolve(root, name)
		}
		else{
			url = new URL(name, root).href
		}

		let comp = url.startsWith("/virtual/kowix")
		let mode = 1
		if(Os.platform() == "win32"){
			if(!comp){
				comp || url.substring(2).startsWith("\\virtual\\kowix")
				mode = 2 
			}
		}

		if(comp){
			//url = Path.join(__dirname, "kowix") + url.substring(14)
			// method from kowix 


			


			let kowixCtx = this.getSiteContext("kowix")
			let rurl = url.substring(15)
			if(Os.platform() == "win32"){
				rurl = url
				if(mode == 2){
					rurl = url.substring(2)	
				}
				rurl = rurl.replace(/\\/g, '/').substring(15)
			}
			let method = kowixCtx.dynamicMethod(rurl)
			method.siteContext = this
			return method 
		}
		
		// new URL? 
		return new DynamicMethod(this, url, name)	
	}


	userFunction(name: string){
		let folder = this.site.config.kowix?.functions || "./functions"
		return this.dynamicMethod(Path.posix.join(folder, name))
	}

	UserFunction(name){ return this.userFunction(name) }


	getSite(){
		return this.site
	}

	get idSite(){
		return this.site.id
	}

	get IdSite(){
		return this.site.id
	}

	get constants(){
		return this.site["$_constants"] || {}
	}

	get context() : HttpContext{
		return this.data.context
	}

	uniqid(){
		return uniqid()
	}

	get Exception(){
		return Exception
	}



	get publicContext(){
		if(publicContext[this.site.id] === undefined){
			publicContext[this.site.id] = {}
		}
		return publicContext[this.site.id]
	}


	getSitesConfig(){
		return [ ... this.data.sites.map((a) => a.config) ]
	}

	getSiteContext(id: string): SiteContext{
		
		let sites = this.data.sites.filter((a) => a.id == id)
		if(sites.length){
			return sites[0].getKiwiContext(this.data)
		}
	}

	getHomeDir(): string{
		return this.site.userFolder
	}

	get UserContext(){
		return this
	}



	async resolve(name: string){

		let mod = await import(kawix.packageLoader)
		let reg = new mod.Registry()
		return reg.resolve(name)

	}

	async require(name: string){
		let mod = await import(kawix.packageLoader)
		let reg = new mod.Registry()
		return reg.require(name)
	}

}

export class DynamicMethod{
	name: string
	site: Site 
	siteContext: SiteContext
	url: string 

	importInfo: any 
	resolver: any 
	mod: any 


	static methodCache = new Map<string, any>()

	constructor(siteContext: SiteContext, url: string, name: string){
		if(name.startsWith("./")) name= name.substring(2)
		this.name = name
		this.siteContext = siteContext
		this.site = siteContext.site
		this.url = url
	}


	async loadCache(){
		let cached = DynamicMethod.methodCache.get(this.url)
		
		if(cached?.time && (Date.now() - cached.time > 15000)){
			//cached = null
		}
		let importInfo = null, resolver = null
		if(!cached){
			if(this.site.config.methodResolver){				
				importInfo = await this.site.config.methodResolver.importInfo(this.name)
				console.info("Resolve:", this.name, importInfo)
				if(importInfo) resolver = this.site.config.methodResolver
			}

			if(!importInfo){
				importInfo = await kawix.importInfo(this.url)
			}
			cached = {
				time: Date.now(),
				importInfo,
				resolver
			}
			DynamicMethod.methodCache.set(this.url, cached)
		}


		this.importInfo = cached.importInfo
		this.resolver = cached.resolver
	}


	async isAvailable(){
		try{
			await this.loadCache()
			return true
		}catch(e){
			if(e.code === "MODULE_NOT_FOUND"){
				return false
			}
			throw e 
		}		
	}

	async load(){
		await this.loadCache()
		if(this.resolver){
			this.mod = await this.resolver.importFromInfo(this.importInfo)
		}
		else{
			this.mod = await kawix.import(this.url)
		}
		if(this.mod["kawixDynamic"] === undefined){ 
			this.mod["kawixDynamic"] = {
				time: 15000
			}
		}
	}


	async invoke(params?: {[key: string]: any}): Promise<any>{
		await this.load()
		let method = null
		let methodname = this.siteContext.context.request?.method?.toUpperCase()
		if(methodname && this.mod.httpActions){
			method = this.mod.httpActions[methodname] || this.mod.httpActions.ALL
		}
		if(!method){
			method = this.mod.httpInvoke || this.mod.kowixInvoke || this.mod.invoke 
		}
		if(method){
			return method.call(this, this.siteContext, params)
		}
		return this.mod.default || this.mod
	}


}