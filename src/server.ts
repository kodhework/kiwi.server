import * as async from 'gh+/kwruntime/std@4fe87b1/util/async.ts'
import {Server} from '/data/projects/Kode/kwruntime/std/http/server.ts'
import {HttpContext} from '/data/projects/Kode/kwruntime/std/http/context.ts'
import {JsonParser} from '/data/projects/Kode/kwruntime/std/http/parsers/json.ts'
import {FormEncoded} from '/data/projects/Kode/kwruntime/std/http/parsers/form-encoded.ts'
import {Site} from './site.ts'
import {parse} from "gitlab://jamesxt94/codes@465eb5ab/cli-params.ts"
//import * as kowixConfig from '../kowix/config.ts'

import Path from 'path'
import Url from 'url'
import { AddressInfo } from 'net'

export interface SiteByUrl{
	url: string
	root?: string 
}

let share = Symbol("share")

export class Program {

	static default = new Program()
	address: string | AddressInfo
	#server: Server 
	sites = new Array<Site>(Site.kowixSite)
	#preloaded = new Set<string>()


	static async main(){
		let cli = parse()
		return Program.default.main(cli.params.address)
	}

	async main( address = "tcp://127.0.0.1:40800"){
		let server = new Server()
		let addr = await server.listen(address)
		this.address = addr 
		console.info("App started on address:", addr)
		server.bodyParsers.set("application/json", JsonParser)
		server.bodyParsers.set("application/x-www-form-urlencoded", FormEncoded)
		this.#server = server 

		//Program.sites.push(new Site(config.site))
		//Program.sites.push(new Site(kowixConfig.site))
		this.beginAccept()
		return server 
	}	

	constructor(){
		this[share] = true
	}

	deleteSite(id: string){
		let index = -1
		for(let i=0;i<this.sites.length;i++){
			let site = this.sites[i]
			if(site.id == id){
				index = i
				break 
			}
		}	
		if(index >= 0) this.sites.splice(index, 1)
	}

	deleteSites(ids: Array<string>){
		for(let id of ids) this.deleteSite(id)
	}

	async addSiteByURL(url: string, root: string){

		console.info("Adding here ---", url)
		let config = await import(url)
		if(!config.kawixDynamic){
			config.kawixDynamic = {
				time: 10000
			}
		}
		console.info(config.kawixDynamic)
		if(config.site){
			this.addSite(config, root)
		}

	}


	async addSitesByURL(items: Array<SiteByUrl>){
		for(let item of items){
			await this.addSiteByURL(item.url, item.root)
		}
	}	

	async preload(url: string){
		if(this.#preloaded.has(url)) return 
		await import(url)
		this.#preloaded.add(url)
	}

	addSite(config: any, root: string = ''){
		let index = -1, site: Site = null
		if(config.site){
			for(let i=0;i<this.sites.length;i++){
				site = this.sites[i]
				if(site.id == config.site.id){
					index = i
					break 
				}
			}			


			
			if(!config.site.root){
				config.site.root = root
			}

			if(index >= 0){	

				if(config.site.$loaded){
					return 
				}
				
				console.info("[KiwiServer]", new Date(), "Adding site:", config.site.id)
				site = new Site(config.site)
				this.sites[index] = site
			}
			else{
				console.info("[KiwiServer]", new Date(), "Adding site:", config.site.id)
				site = new Site(config.site)
				this.sites.push(site)
			}
			config.site.$loaded = true
		}
		return site
	}


	
	async startReadConfig(url: string){

		if(Path.isAbsolute(url) || (url.indexOf(":") < 0)){
			url = Url.pathToFileURL(url).href
		} 
		
		while(true){
			try{

				let mod = await import(url)
				if(!mod.kawixDynamic){
					mod.kawixDynamic = {
						time: 10000
					}
				}
				let config = mod.default || mod.config || mod 
				if(config.includes instanceof Array){

					let currentSiteIds = []
					for(let include of config.includes){


						let resolved = new URL(include, url).href
						try{
							let nmod = await import(resolved)
							if(nmod.site){
								currentSiteIds.push(nmod.site.id)
								let parts = resolved.split("/")
								parts.pop()
								await this.addSite(nmod, parts.join("/"))
							}
						}catch(e){
							console.error("[KiwiServer] Failed load site:", e)
						}
					}
				}
			}
			catch(e){
				console.error("Failed to get config url:", e.message)	
			}finally{
				await async.sleep(10000)
			}
		}

	}



	async beginAccept(){

		let iterator = this.#server.getIterator(["error","request", "upgrade"])
		for await(let event of iterator){

			if(event.type == "error"){
				console.error("[KiwiServer]", new Date(), "Web server error:", event.data)
			}
			else{
				this.handle(event.data)
			}
		}

	}

	
	async handle(ctx: HttpContext){
		
		console.info("[KiwiServer]", new Date(), ctx.request.method, ctx.request.url)
		try{
			

			let possibles = new Array<any>()
			for(let site of this.sites){
				let res = site.check(ctx)
				if(res.ok){
					possibles.push({
						site, 
						res,
						relevance: res.prefix ? 1 : 0
					})
				}
			}

			// primero prefixes, luego hostnames 
			possibles.sort((a,b) => {
				let num = (b.site.config.relevance||0) - (a.site.config.relevance||0)
				if(num == 0){
					num= b.relevance - a.relevance
				}
				return num
			})


			
			let pos = possibles[0]
			if(pos){
				let site = pos.site as Site
				let siteContext = site.getKiwiContext({
					context: ctx,
					sites: this.sites
				})
				await siteContext.preload()				
				if(pos.res.offset)
					ctx.request["$seturl"](ctx.request.url.substring(pos.res.offset) || "/")


				ctx["siteContext"] = siteContext
				await site.router.lookup(ctx)

				/*
				if(ctx.request.data["_continue"]){
					let methods = ctx.request.data["_continue"]
					delete ctx.request.data["_continue"]
					
					for(let method of methods){	
						let res = await method(ctx, siteContext)
						if(res === undefined) res = null
						if(!ctx.reply.raw.writableEnded){
							await ctx.reply.send(res)
						}
					}
				}
				*/
			}

			if(ctx.socket){
				if(!ctx.socket.writableEnded){
					ctx.socket.end()
				}
			}
			else if(!ctx.reply.raw.writableEnded){
				await ctx.reply.code(404).send({
					error: {
						code: "NOT_FOUND",
						message: "URL Not found",
						url: ctx.request.uri.pathname
					}
				})
			}
		}catch(e){

			if(ctx.socket){
				if(!ctx.socket.writableEnded){
					ctx.socket.end()
				}
			}

			else if(!ctx.reply.raw.writableEnded){
				await ctx.reply.code(500).send({
					error:{
						code: e.code,
						message: e.message,
						url: ctx.request.uri.pathname
					}
				})
			}

			console.error("[KiwiServer]", new Date(), "Failed request:", e)
		}
	}


}

//export var dynwHandler = Program.default.process.bind(Program.default)