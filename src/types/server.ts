import { HttpContext } from '/data/projects/Kode/kwruntime/std/http/context.ts';
export interface SiteByUrl {
    url: string;
    root?: string;
}
export declare class Program {
    #private;
    static default: Program;
    sites: Site[];
    static main(): Promise<any>;
    main(address?: string): Promise<any>;
    constructor();
    deleteSite(id: string): void;
    deleteSites(ids: Array<string>): void;
    addSiteByURL(url: string, root: string): Promise<void>;
    addSitesByURL(items: Array<SiteByUrl>): Promise<void>;
    preload(url: string): Promise<void>;
    addSite(config: any, root?: string): Site;
    startReadConfig(url: string): Promise<void>;
    beginAccept(): Promise<void>;
    handle(ctx: HttpContext): Promise<void>;
}
