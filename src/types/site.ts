import { HttpContext } from '/data/projects/Kode/kwruntime/std/http/context.ts';
import { Router } from '/data/projects/Kode/kwruntime/std/http/router.ts';
export declare class Site {
    #private;
    config: any;
    root: string;
    router: Router;
    userFolder: string;
    static kowixSite: Site;
    constructor(config: any);
    get id(): any;
    check(ctx: HttpContext): {
        ok: boolean;
        offset: number;
        prefix: boolean;
    };
    buildRouter(): void;
    getKiwiContext(data?: any): SiteContext;
}
export declare class SiteContext {
    site: Site;
    data: any;
    constructor(site: Site, data: any);
    preload(): Promise<void>;
    addGlobal(name: string, value: any): void;
    dynamicMethod(name: string): any;
    userFunction(name: string): any;
    UserFunction(name: any): any;
    getSite(): Site;
    get idSite(): any;
    get IdSite(): any;
    get constants(): any;
    get context(): HttpContext;
    uniqid(): any;
    get Exception(): any;
    get publicContext(): any;
    getSitesConfig(): any[];
    getSiteContext(id: string): SiteContext;
    getHomeDir(): string;
    get UserContext(): this;
    resolve(name: string): Promise<any>;
    require(name: string): Promise<any>;
}
export declare class DynamicMethod {
    name: string;
    site: Site;
    siteContext: SiteContext;
    url: string;
    importInfo: any;
    resolver: any;
    mod: any;
    static methodCache: Map<string, any>;
    constructor(siteContext: SiteContext, url: string, name: string);
    loadCache(): Promise<void>;
    isAvailable(): Promise<boolean>;
    load(): Promise<void>;
    invoke(params?: {
        [key: string]: any;
    }): Promise<any>;
}
