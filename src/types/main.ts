import { Program as Program2 } from './server.ts';
import { Manager, Config } from 'github://kwruntime/dynw@6608707/manager.ts';
import { HttpContext } from '/data/projects/Kode/kwruntime/std/http/context.ts';
import { RPC } from 'gitlab://jamesxt94/mesha@2.0.3/src/rpc.ts';
import { Site } from "./site";
export declare var ServerProgram: any;
export declare class RemoteExecutor {
    static rpc: RPC;
    static connectMesha(address: string, password: string): Promise<any>;
}
export declare class Program {
    #private;
    manager: Manager;
    sitesById: Map<string, any>;
    static startupMode: string;
    static get default(): Program;
    static main(args: Array<string>): Promise<void>;
    static get typeDefinition(): string;
    constructor();
    main(args: Array<string>): Promise<any>;
    shareProcessInfo(id: any): {
        address: string;
        password: string;
    };
    startCluster(program: Program2): Promise<void>;
    executeSiteDynamicMethod(siteId: string, method: string, params: any): any;
    executeSiteFunction(siteId: string, func: string, params: any): any;
    getSiteById(id: string): Site;
    getSites(): Site[];
    startCron(): Promise<void>;
    startReadConfig(url: string): Promise<void>;
    startShareProcess(params: any): Promise<void>;
    getStr(func: Function): string;
    createServerManager(config: Config): Promise<any>;
}
export declare function prepareServer(): Promise<void>;
export declare function defaultHandler(ctx: HttpContext): Promise<any>;
