// A class for manager tmux
import {Client} from 'gitlab://jamesxt94/mesha@2.0.3/src/clients/default.ts'
import {RPC} from 'gitlab://jamesxt94/mesha@2.0.3/src/rpc.ts'

import {Manager, Config, Program as ManagerProgram} from  'github://kwruntime/dynw@6608707/manager.ts'
//"/data/projects/Kodhe/kwruntime/dynw/manager.ts" 
//'github://kwruntime/dynw@1.0.1/manager.ts'

//import {Manager, Config, Program as ManagerProgram} from  
import {Tmux} from "gitlab://jamesxt94/tmux@6187c824/src/v2/tmux.ts"
import {Program} from "gitlab://jamesxt94/tmux@6187c824/src/v2/program.ts"
import * as async from "gh+/kwruntime/std@1.1.19/util/async.ts"
import {spawn} from "child_process"

export class TmuxActions{


    static async execute(params: any){
		let rpc: RPC
		let res = Manager.getLocalId(params.id || "default") 
		let connect = async (address: string, password?: string)=>{
			try{
				let client = new Client()				
				await client.connect(address)
				
				rpc = client.rpc
				let getObject = await rpc.defaultScope.remote("getObject")
				let tmux: Tmux = await getObject.invoke(password)
				return tmux 
			}catch(e){
				if(e.code == "ENOENT" || e.code =="EREFUSED"){
					return null
				}
				throw e 
			}
		}

		//await connect("local+" + res.meshaId)
		let program = new Program()
		if(params.show !== undefined){
			// mostrar la lista de procesos...
			let args = [
				"--",
				"id=" + res.uid,
				"list"
			]
			await program.main(args)
		}


		else if(params.tmux !== undefined || params.control !== undefined){
			let newparams = Object.assign({}, params)
			delete newparams.id
			delete newparams.control 

			let args = [
				"--",
				"id=" + res.uid,
				"base64=" + Buffer.from(JSON.stringify(newparams)).toString("base64")
			]
			await program.main(args)
		}

		else if(params.close !== undefined){

			let tmux: Tmux 
			try{
				tmux = await connect("local+"+res.meshaId)
			}catch(e){
				console.warn("> Service is not online or is not responsive:", e.message)
				return -1 
			}

			if(tmux){
				console.info("Closing service")
				await tmux.close()
				return 
			}
			return 
		}


		else if(params.restart !== undefined){

			let tmux: Tmux 
			try{
				tmux = await connect("local+"+res.meshaId)
			}catch(e){
				console.warn("> Service is not online or is not responsive:", e.message)
				console.info("Please use 'start' command instead")
				return -1 
			}

			if(tmux){
				let pro = await tmux.getProcess("share")
				let info = await pro.info
				let base64 = info.env.KIWI_SERVER_CLI_BASE64
				await tmux.close()
				console.info("> Service closed.")

				
				let p = spawn(process.argv[0], [process.argv[1], process.argv[2], "--", "base64=" + base64], {
					stdio:'ignore',
					detached: true
				})
				p.unref()

				console.info("Starting kiwi.server in background.")
				console.info("You can check the status later")
				return 

			}

			return 

		}

		else if(params.reload !== undefined){
			
			console.info("> Reloading webserver")
			let tmux = await connect("local+"+res.meshaId)

			if(!params.name){
				let pros = await tmux.getProcesses()
				for(let pro of pros){
					let restart = false 
					if(pro.id.startsWith("ws")){
						restart = true 
					}

					if(pro.id == "share"){
						if(params.share !== undefined || params.all !== undefined){
							restart = true 
						}

					}

					if(pro.id == "caddy"){
						if(params.caddy !== undefined || params.all !== undefined){
							restart = true 
						}
					}


					if(restart){
						console.info("> Restarting cluster:", pro.id)
						let opro = await tmux.getProcess(pro.id)
						await opro.kill('SIGTERM', 5000)
						await async.sleep(1000)
					}
					
				}
			}
			else{

				let names = params.name
				if(!(names instanceof Array)){
					names = names.split(",")
				}
				for(let id of names){
					console.info("> Restarting cluster:", id)
					let opro = await tmux.getProcess(id)
					await opro.kill('SIGTERM', 5000)
					await async.sleep(100)

				}

			}

			rpc?.stream.destroy()
			

		}

		else if(params.control !== undefined){
			let newparams = Object.assign({}, params)
			delete newparams.id
			delete newparams.control 

			let args = [
				"--",
				"id=" + res.uid,
				"base64=" + Buffer.from(JSON.stringify(newparams)).toString("base64")
			]
			await program.main(args)
		}

    }

}