### Configuration file

Configuration file defines what projects (```sites```) will be included in webserver. Basically only requires a exported field: ```includes```

Example:

```typescript
import Path from 'path'

export var includes = [
    Path.join(__dirname, "site1", "config.ts"),
    Path.join(__dirname, "site2", "config.ts")
]
```

You can add a mechanism for auto-update the sites included. This will work:


```typescript
import Path from 'path'
export var includes = []

setInterval(readSitesFromSomewhere, 10000)
async function readSitesFromSomewhere(){

    includes.splice(0, includes.length)

    // Here all your logic 
    includes.push("/path/to/site1/config.ts")
    includes.push("/path/to/site2/config.ts")

}
```

Configuration file, only defines what sites will be added to webserver, but each site should define a configuration file. 



### Site configuration file


Site configuration files, defines the routes, prefixes, and how process. 

You should export the configuration as field ```site```.


```typescript

export var site = {
    // a unique id/name
    id: "com.kodhe.test",
    name: "com.kodhe.test",

    // global prefixes. By default, this has more relevance than hostname
    // this means, that you can call many projects from an unique domain
    // kiwi.server will redirect the request to this site
    globalprefixes: [
        "/com.kodhe.test",
        "/test-example"
    ],

    // when hostname matches, kiwi.server will redirect the request to this site
    hostnames: [
        "test.kodhe.work"
    ],

    // a file to execute before any request
    preload: [
        "./src/loader"
    ],

    // you can define routes here
    routes: [

        // static routes 
        {
			"path": "/static"
			"static": Path.join(__dirname, "public")
			// required for middlewares, static 
			"method": "use"
		},


        // you can use wildcards
        {
			"path": "/api/v1/*"

            // you can set middlewares
			middleware: 
				file: "./src/allow"

            // this file will be executed when route matches.
            //
            // /virtual/kowix is an alias for 'kowix' site
            // (included in this project, check the kowix folder)
            // So /virtual/kowix/src/functions will be translated to ${KIWISERVERFOLDER_OR_URL}/kowix/src/functions.ts
            // This configuration is special, 
            // will look for the wildcard, inside folder
            // specified in field: kowix.functions 
            // 
            // In this example, if the request url is: /com.kodhe.test/api/v1/user/read
            // kiwi.server will look for and execute the file:
            // ./src/webapi/user/read.ts (.js, or any other valid extension)
			"file": "/virtual/kowix/src/functions"
		},

        // or why not, use named parameters
        {
			"path": "/api/book/:id"
			middleware: 
				file: "./src/allow"
			"file": "./src/webapi/book"
		},


    ],


    kowix: {
        // path to the folder containing all files will be called
        // in the route wildcard match, when file is setted to
        // /virtual/kowix/src/functions
        functions: "./src/webapi"
    },

    // yes, kiwi.server can execute crons, by default each minute
    cron: "./src/webapi/cron.ts",

    defaultroute: {
        // if the url doesn't match any route configured here,
        // but matchs the prefixes or the hostname.
        //
        // /virtual/kowix/src/default is an special file, and will
        // look for file api.default inside the folder
        // specified in field: kowix.functions 
        //
        // In this example, should be: ./src/webapi/api.default.ts (.js or any other valid extension)
    
        file: '/virtual/kowix/src/default'
    }		


}
```

You can see, any route define a ```file``` to be executed.  We will understand how works: 


## Files to be executed

A file to be executed, is basically a javascript/typescript file (or any other valid) that exports some of this signatures:

> See the types defined on [./src/types](./src/types/)

```typescript 

export var httpActions = {
    async POST(site: SiteContext, params: any): Promise<any>{
        // executed on POST requests
    },

    async GET(site: SiteContext, params: any): Promise<any>{
        // executed on GET requests
    },

    // etc 
}

export async function kowixInvoke(siteContext: SiteContext, params: any): Promise<any>{
    // execute on any request type

    // you can check the method request: 

    if(site.context.request.method == "POST"){
        // ....
    }
}

``` 

- **kiwi.server** will be look for ```httpActions``` exported variable. If foun, will search function called with the request method (POST, GET, DELETE, etc) or ALL if none matches.
- **kiwi.server** if previous step no return any function, will search ```kowixInvoke``` exported function.
- **kiwi.server** if previous step no return any function, will be returned the default exports.


The function signature can returns a value of any type. Will be converted to ```JSON``` automatically, without any additional effort.

If you need raw control over the ```request/response``` objects, you can: 

```typescript

export async function kowixInvoke(site: SiteContext, params: any): Promise<any>{
    
    let rawNodeJsRequest = site.context.request.raw
    let rawNodeJsResponse = site.context.reply.raw

    // some heavy code

    // suppose you need pipe an stream
    stream.pipe(rawNodeJsResponse)
    return new Promise(function(a,b){
        rawNodeJsResponse.on("finish", a)
        rawNodeJsResponse.on("error", b)
    })

}
```

However, you can manipulate the ```reply``` object instead of raw response object. For example, this is equivalent: 


```typescript

export async function kowixInvoke(site: SiteContext, params: any): Promise<any>{
    
    // some heavy code

    // pipe the stream to response
    await site.context.reply.send(stream)

}
```

For more documentation about the httpcontext object (contains: reply, request) see: [kwruntime/std http/context](https://github.com/kwruntime/std/blob/main/http/context.ts)



### Watch folder instead of configuration file

You can start **kiwi.server** and instruct to watch a folder for sites will be attached.

```bash
kiwi.server -- start clusters=2 port=8080 host=127.0.0.1 folder=/path/to/sites_folder
```
**kiwi.server** will search changes periodically in /path/to/sites_folder , to add/delete site configurations at runtime. The current supported extensions are: ```.ts```,  ```.kwb```, ```.kwc```.

> .kwb and .kwc are special extensions for packed modules. You can see [CHANGES in kwruntime/core](https://github.com/kwruntime/core/blob/main/CHANGES.md#1114) for more information