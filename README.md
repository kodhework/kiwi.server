# kiwi.server

**kiwi.server**  is a fast web server for nodejs, using [kwruntime/core](https://github.com/kwruntime/core).

- Allow host multiple projects in only one process/web server.
- Builtin cluster support.
- Builtin RPC comunication channel.
- Reload webapi functions in run-time (no need restart webserver).
- Start in background, like a service.
- Can acts directly as Reverse Proxy (using caddy internally).
- Functions for reload/restart webserver.
- Builtin application/json, application/x-form-www-url-encoded parsers. 


## How works? 

**kiwi.server** works basically in two parts: webserver - projects attached (sites).

A simple explanation how works: 

- Create processes (we call cluster) for each cpu (or as configured).
- Each cluster creates a unix socket server (http server on windows), that process all incoming requests, by default, any route will show error 404. See [kwruntime std/http/server](https://github.com/kwruntime/std/blob/main/http/server.ts).
- Create a process for ```manage``` all clusters and for interprocess comunication. We call ```share``` process. This process using a pure js protocol for IPC. See [mesha](https://gitlab.com/jamesxt94/mesha/-/tree/04ca2175f45cdf406a0ffb48b5d67762e5a3e038).
- Start a **Caddy** instance as Reverse Proxy, balanced to clusters created. See [kwruntime/dynw](https://github.com/kwruntime/dynw).
- **Caddy** supports SSL, and will be run with ```sudo``` if requested. (run with ```sudo``` should work but is not fully tested).
- ```share``` process will check a **configuration file** that will contains all the projects config files (we call ```sites```) you need attach to webserver.
- You can ```add``` or ```delete``` any project in your configuration file and should be reflected, without reload. However, for any reason you can force reload using ```--reload``` command 
- You can modify a file in your projects, and changes will be propagated to webserver, without reload. 
- ```share``` process can watch a folder for ```.ts```, ```.kwb``` or ```.kwc``` files instead of read a configuration file.


# More info

- [Documentation](./DOCS.md)
- [Cli interface](./cli.md)
