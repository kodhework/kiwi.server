import { kowixInvoke as kowixInvoke2} from "./functions.ts"

export async function kowixInvoke(local, body){
	if(local.context.request?.params){
		local.context.request.params.wild = "api.default"
	}
	return await kowixInvoke2(local, body)
}