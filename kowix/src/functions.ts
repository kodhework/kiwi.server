import { SiteContext } from "../../site"

export async function kowixInvoke(local:SiteContext, body){	

	let input:any = null
	if(local.site.config.parse !== false )
		input = await local.context.request.body.tryParse()
	body = Object.assign({}, local.context.request.query || {}, input?.data || {})

	
	let wild = local.context?.request?.params?.wild 
	if(!wild){
		throw local.Exception.create("Website URL not found").putCode("NOT_FOUND")
	}

	let func= local.userFunction(wild)
	if(await func.isAvailable()){
		if((Date.now() - (local.site["$_updated"]||0)) > 15000){
			try{
				let result = await local.userFunction("vars").invoke({})
				local.site["$_constants"] = result.constants || result
			}catch(e){
			}
			finally{
				local.site["$_updated"] = Date.now()
			}
		}
		//local.constants = local.site["$_constants"] || {}


		// process body
		if(local.constants.bodyAnalyze && Object.keys(body).length > 0){
			body = await local.userFunction(local.constants.bodyAnalyze).invoke(body)
		}

		let result = await func.invoke(body)
		if(local.constants.bodyTransform){
			result = await local.userFunction(local.constants.bodyTransform).invoke(result)
		}
		return result
	}
	else{
		throw local.Exception.create(`Website Function for '${wild}' not found`).putCode("NOT_FOUND")
	}

}