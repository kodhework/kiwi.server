export class DynamicMethods{
root = "/data/files/projects/Kode/kiwi.server/kowix"
	private available = ["src/allow.ts","src/allow","src/core-backward/Exception.ts","src/core-backward/Exception","src/core-backward/Fs.ts","src/core-backward/Fs","src/core-backward/Moment.ts","src/core-backward/Moment","src/core-backward/Request.ts","src/core-backward/Request","src/core-backward/Task.ts","src/core-backward/Task","src/core-backward/mod.ts","src/core-backward/mod","src/default.ts","src/default","src/functions.ts","src/functions","src/legacycore.ts","src/legacycore","functions/SharedMemory/channel.ts","functions/SharedMemory/channel","functions/db/insert.ts","functions/db/insert","functions/db/query.ts","functions/db/query","functions/db/remove.ts","functions/db/remove","functions/db/update.ts","functions/db/update","functions/db.insert.ts","functions/db.insert","functions/db.query.ts","functions/db.query","functions/db.remove.ts","functions/db.remove","functions/db.update.ts","functions/db.update","functions/headers.ts","functions/headers","functions/time.ts","functions/time"]

	importInfo(name: string){
		if(this.available.indexOf(name) >= 0){  return {type:'internal', name} } 
	}
	importFromInfo(info: any){
		return this[info.name]
	}
	import(name: string){
		return this[name]
	}

	get ['src/allow.ts'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/src/allow.ts")
	}
	get ['src/allow'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/src/allow.ts")
	}


	get ['src/core-backward/Exception.ts'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/src/core-backward/Exception.ts")
	}
	get ['src/core-backward/Exception'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/src/core-backward/Exception.ts")
	}


	get ['src/core-backward/Fs.ts'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/src/core-backward/Fs.ts")
	}
	get ['src/core-backward/Fs'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/src/core-backward/Fs.ts")
	}


	get ['src/core-backward/Moment.ts'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/src/core-backward/Moment.ts")
	}
	get ['src/core-backward/Moment'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/src/core-backward/Moment.ts")
	}


	get ['src/core-backward/Request.ts'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/src/core-backward/Request.ts")
	}
	get ['src/core-backward/Request'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/src/core-backward/Request.ts")
	}


	get ['src/core-backward/Task.ts'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/src/core-backward/Task.ts")
	}
	get ['src/core-backward/Task'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/src/core-backward/Task.ts")
	}


	get ['src/core-backward/mod.ts'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/src/core-backward/mod.ts")
	}
	get ['src/core-backward/mod'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/src/core-backward/mod.ts")
	}


	get ['src/default.ts'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/src/default.ts")
	}
	get ['src/default'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/src/default.ts")
	}


	get ['src/functions.ts'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/src/functions.ts")
	}
	get ['src/functions'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/src/functions.ts")
	}


	get ['src/legacycore.ts'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/src/legacycore.ts")
	}
	get ['src/legacycore'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/src/legacycore.ts")
	}


	get ['functions/SharedMemory/channel.ts'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/functions/SharedMemory/channel.ts")
	}
	get ['functions/SharedMemory/channel'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/functions/SharedMemory/channel.ts")
	}


	get ['functions/db/insert.ts'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/functions/db/insert.ts")
	}
	get ['functions/db/insert'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/functions/db/insert.ts")
	}


	get ['functions/db/query.ts'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/functions/db/query.ts")
	}
	get ['functions/db/query'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/functions/db/query.ts")
	}


	get ['functions/db/remove.ts'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/functions/db/remove.ts")
	}
	get ['functions/db/remove'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/functions/db/remove.ts")
	}


	get ['functions/db/update.ts'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/functions/db/update.ts")
	}
	get ['functions/db/update'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/functions/db/update.ts")
	}


	get ['functions/db.insert.ts'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/functions/db.insert.ts")
	}
	get ['functions/db.insert'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/functions/db.insert.ts")
	}


	get ['functions/db.query.ts'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/functions/db.query.ts")
	}
	get ['functions/db.query'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/functions/db.query.ts")
	}


	get ['functions/db.remove.ts'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/functions/db.remove.ts")
	}
	get ['functions/db.remove'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/functions/db.remove.ts")
	}


	get ['functions/db.update.ts'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/functions/db.update.ts")
	}
	get ['functions/db.update'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/functions/db.update.ts")
	}


	get ['functions/headers.ts'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/functions/headers.ts")
	}
	get ['functions/headers'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/functions/headers.ts")
	}


	get ['functions/time.ts'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/functions/time.ts")
	}
	get ['functions/time'](){
		return import("/data/files/projects/Kode/kiwi.server/kowix/functions/time.ts")
	}

}