import Path from 'path'
import fs from 'fs'

export class Program{
	static async main(){
		

		let webapi = Path.join(__dirname, "src")
		let functions = Path.join(__dirname, "functions")

		let code = ["export class DynamicMethods{","", "", ""]
		code[1] = "root = " + JSON.stringify(__dirname)
		code.push("	importInfo(name: string){")
		code.push("		if(this.available.indexOf(name) >= 0){  return {type:'internal', name} } ")
		code.push("	}")

		code.push("	importFromInfo(info: any){")
		code.push("		return this[info.name]")
		code.push("	}")

		code.push("	import(name: string){")
		code.push("		return this[name]")
		code.push("	}")


		let availables = []
		await this.check(webapi, code, availables, 'src')
		await this.check(functions, code, availables, 'functions')
		code[2] = `\tprivate available = ${JSON.stringify(availables)}`
		code.push("}")

		await fs.promises.writeFile(Path.join(__dirname, "dynamicMethods.ts"), code.join("\n"))

	}


	static async check(folder: string, code = [], availables = [], prefix= ''){

		let files = await fs.promises.readdir(folder)
		for(let name of files){
			let file = Path.join(folder, name)
			let stat = await fs.promises.stat(file)
			if(stat.isDirectory()){
				await this.check(file, code, availables, Path.posix.join(prefix, name))
			}
			else{
				let tcode = "		return import("+JSON.stringify(file)+")"
				let fname1 = Path.posix.join(prefix, name) 
				console.info("fname:", fname1)
				if(!fname1.endsWith(".ts") && !fname1.endsWith(".js")){
					// un import de contenido?
					let content = await fs.promises.readFile(file)
					tcode = `		return {content: Buffer.from(${JSON.stringify(content.toString('base64'))},'base64')}`
				}
				let fname2 = Path.join(prefix, Path.basename(fname1, Path.extname(fname1)))
				code.push("")
				code.push("	get ['" + fname1 + "'](){")
				code.push(tcode)
				code.push("	}")
				availables.push(fname1)
				if(fname2 != fname1){
					code.push("	get ['" + fname2 + "'](){")
					code.push(tcode)
					code.push("	}")
					availables.push(fname2)
				}
				code.push("")
			}
		}

		return code 
	}
}