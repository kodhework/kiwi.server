let parts = import.meta.url.split("/")
parts[parts.length-1]= ''

export var site= {
	"root": parts.join("/"),
	"id": "kowix",
	"hostnames": [
		"kowix.dev",
		/kowix\.kodhe\..*/
	],
	"globalprefixes": [
		"/site/kowix",
		"/dhs.kowix",
		"/dhs/kowix"
	],
	"routes": [
		{
			"path": "/static/e/:id",
			"static": "./public",
			"method": "use"
		},
		{
			"path": "/static",
			"static": "./public",
			"method": "use"
		},
		{
			"path": "/api/v1/*",
			"middleware": {
				"file": "./src/allow"
			},
			"file": "./src/functions"
		}
	]
}
