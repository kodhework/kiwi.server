export var kowixInvoke = function(local, body: any) {
	var headers = local.context.request.headers
	if (headers["x-client-crt"]) {
		headers["x-client-crt"] = decodeURIComponent(headers["x-client-crt"].toString())
	}
	return headers
}