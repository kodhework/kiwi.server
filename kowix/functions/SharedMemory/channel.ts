import { EventEmitter } from "events"
import { SiteContext } from "../../../src/site.ts"
import { Program } from "../../../src/server"
import * as async from "gh+/kwruntime/std@1.1.19/util/async.ts"
import {Exception} from "gh+/kwruntime/std@1.1.19/util/exception.ts"

/*
import {Client, ClientSocket} from 'gitlab://jamesxt94/mesha@3a6411de/Messaging.ts' 
import { RPC } from 'gitlab://jamesxt94/mesha@3a6411de/RPC.ts'
*/


import { RPC } from 'gitlab://jamesxt94/mesha@2.0.3/src/rpc.ts'
import {Client} from 'gitlab://jamesxt94/mesha@2.0.3/src/clients/default.ts'


let share = Symbol("share")

export class Memory extends EventEmitter{
	
	vars : {[key: string]: any}
	memId: string 
	siteContext: SiteContext
	currentSiteContext: SiteContext

	//varContext: {[key: string]: any} = {}


	constructor(siteContext: SiteContext){
		super()

		this[share] = true
		this.siteContext = siteContext
		this.currentSiteContext = siteContext	


		if(siteContext.publicContext.$shared_memory === undefined){
			siteContext.publicContext.$shared_memory = {}
		}


		// INIT VARIABLES
		this.varContext.__channel__memory = this.varContext.__channel__memory || {}
		this.varContext.__channel__memory.vars = this.varContext.__channel__memory.vars || {}
		this.varContext.__channel__memory.locks = this.varContext.__channel__memory.locks || {}

	}

	get varContext(){
		return this.siteContext.publicContext.$shared_memory
	}


	get(name: string){
		let vars = this.varContext
		let value = vars[name]
		if(value === undefined){
			let parts = name.split(".")
			for(let i=0;i<parts.length;i++){
				let part = parts[i]
				if(vars[part] === undefined){
					return undefined 
				}
				vars = vars[part]
			}
			value = vars 
		}		
		return value
	}



	set(name: string, value: any){
		let parts = name.split(".")
		let vars = this.varContext
		let prefix = parts.slice(0, parts.length-1).join(".")

		for(let i=0;i< (parts.length-1);i++){
			let part = parts[i]
			if(vars[part] === undefined){
				vars[part] = {}
			}
			vars = vars[part]
		}

		if(value === null){
			delete vars[parts.pop()] 
		}
		else{
			vars[parts.pop()] = value
		}

		// change 
		let ev = {
			name,
			value,
			time: Date.now()
		}
		this.emit("change", ev)

		let autodelete = this.varContext._autodelete?.[prefix]
		if(autodelete && (typeof vars == "object")){
			let keys = Object.keys(vars)
			if(keys.length > autodelete.maxCount){
				delete vars[keys[0]] 
			}
		}
		return 
	}

	/*
	_setSiteContext(siteId: string){

		
		this.memId = siteId
		this.varContext.__channel__memory = this.varContext.__channel__memory || {}
		this.varContext.__channel__memory.vars = this.varContext.__channel__memory.vars || {}
		this.varContext.__channel__memory.locks = this.varContext.__channel__memory.locks || {}
	
	}*/


	
	setSiteConfig(config:any){	
		
	}
	

	id(){
		if(this.varContext.__channel__memory.id === undefined){
			this.varContext.__channel__memory.id = 0
		}
		return this.varContext.__channel__memory.id++
	}

	lockId(){
		if(this.varContext.__channel__memory.lock_id===undefined){
			this.varContext.__channel__memory.lock_id = 0
		}
		return this.varContext.__channel__memory.lock_id++
	}

	static version(){
		return 3.3
	}

	get version(){
		return Memory.version
	}


	async lock(name: string, timeout: number = 60000, expires: number = 300000){

		let locks = this.varContext.__channel__memory.locks
		let lockid = this.lockId()
		let waiting = locks[name] && locks[name].waiting
		if(Object.keys(waiting).length > 0){
			let def = new async.Deferred<void>()
			locks[name].waiting[lockid] = def
			let timer = setTimeout(def.reject.bind(def, Exception.create("Timeout acquiring lock").putCode("TIMEOUT")))
			try{
				await def.promise
				clearTimeout(timer)
			}catch(e){
				throw e
			}finally{
				delete locks[name].waiting[lockid]
			}
		}
		
		// now adquire lock?? 
		locks[name] = locks[name] || {
			locks: {}
		}
		locks[name].id = lockid

		let locker = {
			id: lockid,
			name,
			timeout,
			expires
		}
		locks[name].timer = setTimeout(this.unlock.bind(this, locker), expires)
		return locker
		
	}


	async unlock(lock){
		let locks = this.varContext.__channel__memory.locks
		let locked = locks[lock.name] 
		
		if(locked.timer){
			clearTimeout(locked.timer)
			delete locked.timer
		}

		if(locked.id == lock.id){

			// UNBLOCK ...
			delete locked.id
			let waiting = locked.waiting
			let keys =Object.keys(waiting)
			let defer = locked.waiting[keys.shift()]
			if(defer){
				defer.resolve()
			}

		}
	}


	async invokeMethod(name: string, vars: any){
		/*
		if(this.sitesConfig[this.memId]){
			let config = {
				site: this.sitesConfig[this.memId]
			}
			Program.default.addSite(config)
		}
			

		
		let sites = Program.default.sites.filter((a) => a.id == this.memId)
		if(sites[0]){
			let ctx = sites[0].getKiwiContext({
				context:{},
				sites: Program.default.sites
			})
			return await ctx.userFunction(name).invoke(vars)
		}
		else{
			throw Exception.create(`Site context ${this.memId} not found`).putCode("SITECONTEXT_NOT_FOUND")
		}*/

		return this.siteContext.userFunction(name).invoke(vars)
	}


	



	enableAutoDelete({prefix, maxCount}){
		let vars = this.varContext
		if(!vars._autodelete) vars._autodelete = {}
		vars._autodelete[prefix] = {
			maxCount
		}

	}

	disableAutoDelete({prefix}){
		let vars = this.varContext
		delete vars._autodelete[prefix]
	}

	// apply autodelete
	autoDelete(){
		let vars = this.varContext
		for(let prefix in vars._autodelete){
			let params = vars._autodelete[prefix]
			let obj = null
			if(prefix)
				obj = this.get(prefix)
			else 
				obj = this.varContext.__channel__memory.vars


			if(obj && (typeof obj == "object")){
				let keys = Object.keys(obj)
				while(keys.length > params.maxCount){
					delete obj[keys[0]]
				}
			}
		}

	}

	
	attachChanges(){	
	}
	stopChanges(){	
	}

	// attach changes over variable 
	attach(name: string, listener: any){	

		if(typeof listener.rpa_preserve == "function"){
			listener.rpa_preserve()
		}

		let emiter:EventEmitter = this
		let x
		
		x = (ev)=>{

			try{
				if(ev.name.startsWith(name + ".") || (ev.name == name)){
					let ev1 = {
						name: ev.name,
						value: this.get(name),
						time: ev.time
					}
					Object.defineProperty(ev1,'rpa_plain',{enumerable: false, value: true})
					listener(ev1)
				}
			}catch(e){
				if(e.code.indexOf("RPA")>=0 && (e.code.indexOf("DESTROY")>=0)){
					emiter.removeListener("change", x)
					listener.rpa_unref && listener.rpa_unref()
				}
				else{
					throw e 
				}
			}
			
		}

		emiter.on("change", x)

	}


}



export class RemoteExecutor{
	static rpc: RPC
	static client: Client

	
	static async connectMesha(address: string, password: string){

		const client = new Client()
		await client.connect(address)
		client.on("error", (e)=> console.error("[kowix] RPC Error", e.message))
		RemoteExecutor.client = client 



		let rpc = client.rpc
		rpc.on("error", (e)=> console.error("[kowix] RPC Error", e.message))
		RemoteExecutor.rpc = rpc 


		const start = await rpc.defaultScope.remote("GetObject")
		return await start.invoke(password)

	}
}



export async function kowixInvoke(local: SiteContext, body: any){


	if(typeof body.site == "string"){
		body.idSite = body.site
	}

	let address = ''
	let password = ''
	// Site contexts 
	let originalSiteContext = local.getSiteContext(body.idSite)	
	let kowix = local.getSiteContext("kowix")
	



	if(!kowix.publicContext.mesha && (process.env.KIWI_SHARE_PROCESS != "1")){

		let constants:any = originalSiteContext.constants
		address = body.address || constants.RPC?.address || process.env.KIWI_SHARE_ADDRESS
		password = body.password || constants.RPC?.password || process.env.KIWI_SHARE_PASSWORD
		if(address && password){
			console.info("[kowix] Connecting to mesha share server:", address, password)
			kowix.publicContext.mesha = await RemoteExecutor.connectMesha(address, password)
			RemoteExecutor.rpc.stream.once("close", function(){
				console.info("[kowix] RPC channel closed")
				kowix.publicContext.mesha = null 
				kowix.publicContext.memoryObjects = {}

				// deleting cache
				if(kowix.publicContext.meshaCache){
					let set: Set<string> = kowix.publicContext.meshaCache
					for(let item of set){
						try{
							let parts = item.split("+")
							console.info("[kowix] RPC Closed, deleting cache. Site:", parts[0], "Name:", parts[1])
							let site = kowix.getSiteContext(parts[0])
							delete site.publicContext[parts[1]]
						}catch(e){
						}
					}
				}


			})
		}
	}
	



	
	kowix.publicContext.memoryObjects = kowix.publicContext.memoryObjects || {}
	let memoryObject = kowix.publicContext.memoryObjects[body.idSite]
	if(!memoryObject){
		if(!kowix.publicContext.mesha && address){
			throw local.Exception.create("Server for memory sharing not available").putCode("MEMORYSHARE_NOT_AVAILABLE")
		}
		if(address){
			let program = await kowix.publicContext.mesha.execute(`return global.kiwiProgram`)
			memoryObject = await program.executeSiteFunction("kowix", "SharedMemory/channel", body)
			kowix.publicContext.memoryObjects[body.idSite] = memoryObject
			

			kowix.publicContext.meshaCache = kowix.publicContext.meshaCache || new Set<string>()
			if(body.cacheName){
				kowix.publicContext.meshaCache.add(body.idSite + "+" + body.cacheName)
			}
			
		}
		else{
			console.info("[kowix] Share memory executing on self process")
			memoryObject = kowix.publicContext.memoryObjects[body.idSite] = new Memory(originalSiteContext)
			kowix.publicContext.memoryObjects[body.idSite] = memoryObject	
		}
	}

	return memoryObject
}