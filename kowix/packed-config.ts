import * as config from './config.ts'
import {DynamicMethods} from './dynamicMethods.ts'
const resolver = new DynamicMethods()
config.site["methodResolver"] = resolver
config.site["root"] = resolver.root

export var site = config.site 