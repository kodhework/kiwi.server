// empaquetar proyecto en 1 solo archivo

import {Packager} from '/data/projects/Kodhe/kwruntime/std/package/compiler/pack.ts'
import {Builder} from '/data/projects/Kodhe/kwruntime/std/package/compiler/build.ts'

import {Program} from './kowix/generate-methods.ts'
import Path from 'path'
import fs from 'fs'
main()
async function main(){

	await Program.main()

	let workingFolder = Path.join(__dirname, "dist")
	if(!fs.existsSync(workingFolder)) fs.mkdirSync(workingFolder)

	/*
	let builder = new Builder({
		target:'node',
		npmExternalModules: [
		]
	})	
	await builder.compile(Path.join(__dirname, "packed-main.ts"))
	await builder.writeTo(Path.join(workingFolder, "com.kodhe.kiwiserver.js"))
	*/
	
	

	let packer = new Packager({
		workingFolder,
		root: __dirname,
		follow: true,
		hash: "com.kodhe.kiwiserver-0.1.4",
		useDataFolder: true,
		main: "packed-main.ts",
		buildOptions: {
			npmExternalModules: [
				"node-pty-prebuilt-multiarch@0.10.1-pre.5",
				"node-pty-prebuilt-multiarch"
			]
		}
	})
	/*
	await packer.add([
		Path.join(__dirname, "kowix", "public")
	])
	*/
	await packer.add([
		Path.join(__dirname, "src", "types")
	], Path.join(__dirname, "src"))

	await packer.addSourceFile(Path.join(__dirname, "packed-main.ts"))
	await packer.writeTo(Path.join(workingFolder, "com.kodhe.kiwiserver.kwb"))

}
