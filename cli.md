# Get started

Can use cloning this repo:

```bash
kwrun src/main -- list_of_commands
```

If you prefer, install globally using the url (without clone):

```bash
kwrun --name=kiwi.server --install=gitlab://kodhework/kiwi.server@1561538d/src/main.ts
```

Now you can use:

```bash
kiwi.server -- list_of_commands
```

Here a basic example of how **kiwi.server** can be started

```bash
kiwi.server -- start clusters=2 port=8080 host=127.0.0.1 sitesConfig=/path/to/config.ts
```

### List of commands

```bash

# start a webserver with configuration file
kiwi.server -- start clusters=2 port=8080 host=127.0.0.1 sitesConfig=/path/to/config.ts

# start a webserver with folder watch
kiwi.server -- start clusters=2 port=8080 host=127.0.0.1 folder=/path/to/folder_containing_sites


# see the processes info
kiwi.server -- show 

# stop webserver
kiwi.server -- close 

# fully restart webserver
kiwi.server -- restart

# reload, usefull for 0 downtime changes
kiwi.server -- reload

# reload custom cluster 
kiwi.server -- reload name=ws0 # or ws1, etc

# reload share process
kiwi.server -- reload name=share 

# view log of any cluster
kiwi.server -- control name=ws0 log # or ws1, etc



## Multiple kiwi.servers using id parameter.
kiwi.server -- id=server1 start clusters=2 port=8081 host=127.0.0.1 sitesConfig=/path/to/config.ts
kiwi.server -- id=server2 start clusters=2 port=8082 host=127.0.0.1 sitesConfig=/path/to/other/config.ts


kiwi.server -- id=server1 show
kiwi.server -- id=server2 show
kiwi.server -- id=server1 reload 

```
