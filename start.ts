import {Program as ProgramM} from './module/src/server'
import Path from 'path'

export var includes = [
	Path.join(__dirname, "..", "kodhe.auth", "backend", "config.ts"),
	Path.join(__dirname, "..", "..", "Kodhe", "iptv", "backend", "config.ts"),
	Path.join(__dirname, "..", "transapalma", "backend", "config.ts")
]

export class Program{
	static async main(){
		await ProgramM.default.main("tcp://127.0.0.1:40800")
		ProgramM.default.startReadConfig(__filename)
	}
}
	
